# Assignment3


## Name
Movieholic API

## Description
Web api and a database that is developed by Entity Framework Code First. This database contains three different tables; Franchise Movies and Characters. Each one of these tables has unique relations. Used swagger as documentation for web api and endpoints.
Movieholic API is developed for the purpose of adding and updating Franchise, Movies and Characters within the database.

Further development is to expand this to not only Franchise, Movie and Character tables, but to other entities such as games, countries etc.

## Visuals
![CharacterEndpoint](./MovieholicApi.png)
![FranchiseEndpoint](./MovieholicApi2.png)
![MovieEndpoint](./MovieholicApi3.png)
![CharacterEndpointGet](./MovieholicApi4.png)
![CharacterEndpointGet](./MovieholicApi5.png)

## Installation
You need these following packages:
- Microsoft.EntityFrameworkCore.Design
- Microsoft.EntityFrameworkCore.SqlServer
- Microsoft.EntityFrameworkCore.Tools
- Microsoft.VisualStudio.Web.CodeGeneration.Design
- AutoMapper.Extensions.Microsoft.DependencyInjection
- Swashbuckle.AspNetCore
- Swashbuckle.AspNetCore.Newtonsoft
- Swashbuckle.AspNetCore.Swagger
- Swashbuckle.AspNetCore.SwaggerGen
- Swashbuckle.AspNetCore.SwaggerUI

